import { Mongo } from 'meteor/mongo';

export const Faqs = new Mongo.Collection('faq');
export const Script = new Mongo.Collection('script');
export const emoImage = new Mongo.Collection('emoImage');
export const Commands = new Mongo.Collection('commands');
